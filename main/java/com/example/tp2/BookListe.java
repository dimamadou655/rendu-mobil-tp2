package com.example.tp2;

import java.util.ArrayList;
import java.util.HashMap;

public class BookListe {
    private static ArrayList<Book> books = init();

    private static ArrayList<Book> init() {
        ArrayList<Book> res = new ArrayList<>();
        res.add(new Book("Rouge Brésil", "J.-C. Rufin", "2003", "roman d'aventure, roman historique", "Gallimard"));
        res.add(new Book("Guerre et paix", "L. Tolstoï", "1865-1869", "roman historique", "Gallimard"));
        res.add(new Book("Fondation", "I. Asimov", "1957", "roman de science-fiction", "Hachette"));
        res.add(new Book("Du côté de chez Swan", "M. Proust", "1913", "roman", "Gallimard"));
        res.add(new Book("Le Comte de Monte-Cristo", "A. Dumas", "1844-1846", "roman-feuilleton", "Flammarion"));
        res.add( new Book("L'Iliade", "Homère", "8e siècle av. J.-C.", "roman classique", "L'École des loisirs"));
        res.add(new Book("Histoire de Babar, le petit éléphant", "J. de Brunhoff", "1931", "livre pour enfant", "Éditions du Jardin des modes"));
        res.add(new Book("Le Grand Pouvoir du Chninkel", "J. Van Hamme et G. Rosiński", "1988", "BD fantasy", "Casterman"));
        res.add(new Book("Astérix chez les Bretons", "R. Goscinny et A. Uderzo", "1967", "BD aventure", "Hachette"));
        res.add(new Book("Monster", "N. Urasawa", "1994-2001", "manga policier", "Kana Eds"));
        res.add(new Book("V pour Vendetta", "A. Moore et D. Lloyd", "1982-1990", "comics", "Hachette"));

        return res;
    }

    public static ArrayList<Book> getAllBooks() {
        return books;
    }

}

