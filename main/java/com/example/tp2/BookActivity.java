package com.example.tp2;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_ROLLBACK;

public class BookActivity extends AppCompatActivity {

    BookDbHelper db;

    Book parcedBook=null;
    EditText nameBook;
    EditText editAuthors;
    EditText editYear;
    EditText editGenres;
    EditText editPublisher;

    Button btnSave;
    View.OnClickListener clickListenner;
    Boolean action_add=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        db = new BookDbHelper(this);

        nameBook = findViewById(R.id.nameBook);
        editAuthors = findViewById(R.id.editAuthors);
        editYear = findViewById(R.id.editYear);
        editGenres = findViewById(R.id.editGenres);
        editPublisher = findViewById(R.id.editPublisher);
        btnSave = findViewById(R.id.button);

        if(getIntent().hasExtra("book")){
            btnSave.setText("MODIFIER");
            action_add=false;
            parcedBook = (Book)getIntent().getExtras().getParcelable("book") ;
            nameBook.setText(parcedBook.getTitle());
            editAuthors.setText(parcedBook.getAuthors());
            editYear.setText(parcedBook.getYear());
            editGenres.setText(parcedBook.getGenres());
            editPublisher.setText(parcedBook.getPublisher());
        }

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(nameBook.getText().toString().isEmpty()){
                    AlertDialog.Builder alert = new AlertDialog.Builder(v.getContext());
                    alert.setTitle("Sauvegarde Impossible");
                    alert.setMessage("Le titre du livre doit être non vide");
                    alert.show();
                }else{
                    if(save()){
                        setResult(1);
                        finish();
                    }else{
                        AlertDialog.Builder alert = new AlertDialog.Builder(v.getContext());
                        alert.setTitle("Sauvegarde Impossible");
                        alert.setMessage("Un livre portant le même nom existe déjà");
                        alert.show();
                    }
                }

            }
        });
    }

    public boolean save(){
        if(action_add)
            return addBook();

        return updateBook();
    }

    public boolean addBook(){
        Book book = new Book();
        book.setTitle(nameBook.getText().toString());
        book.setAuthors(editAuthors.getText().toString());
        book.setGenres(editGenres.getText().toString());
        book.setPublisher(editPublisher.getText().toString());
        book.setYear(editYear.getText().toString());

        if(db.addBook(book)){
            Toast.makeText(this,"Ajouté avec succès", Toast.LENGTH_SHORT).show();
            return true;
        }

        return false;
    }

    public boolean updateBook(){
        parcedBook.setTitle(nameBook.getText().toString());
        parcedBook.setAuthors(editAuthors.getText().toString());
        parcedBook.setGenres(editGenres.getText().toString());
        parcedBook.setPublisher(editPublisher.getText().toString());
        parcedBook.setYear(editYear.getText().toString());

        int r=db.updateBook(parcedBook);

        if(r==1){
            Toast.makeText(this,"Modifié avec succès ", Toast.LENGTH_SHORT).show();
            return true;
        }

        return false;
    }



}



