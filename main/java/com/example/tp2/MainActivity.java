package com.example.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import java.io.Serializable;

public class MainActivity extends AppCompatActivity {
    ListView list;
    BookDbHelper db;
    SimpleCursorAdapter adapter;
    Cursor bookCursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //itemAdapter adapter= new itemAdapter(this,BookListe.getAllBooks());
        Adaptateur adapter = new Adaptateur(this,BookListe.getAllBooks());

        list = findViewById(R.id.listView);
        registerForContextMenu(list);
        populateListView();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,BookActivity.class);
                startActivityForResult(intent,1);
            }
        });

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = (Cursor) list.getItemAtPosition(position);
                Book book = BookDbHelper.cursorToBook(cursor);

                Intent intent = new Intent(MainActivity.this,BookActivity.class);
                intent.putExtra("book",book);
                startActivityForResult(intent,1);
            }
        });

        list.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
               MenuInflater menuInflater = getMenuInflater();
                menuInflater.inflate(R.menu.menu_main,menu);
            }
        });
    }

    public void populateListView(){
        db = new BookDbHelper(this);
        bookCursor = db.fetchAllBooks();
        adapter = new SimpleCursorAdapter(this,R.layout.item_view,bookCursor,new String[]{BookDbHelper.COLUMN_BOOK_TITLE,BookDbHelper.COLUMN_AUTHORS},new int[]{R.id.titre,R.id.auteur},0);
        list.setAdapter(adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        populateListView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
         //Handle action bar item clicks here. The action bar will
         //automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        switch (item.getItemId())
        {
            case R.id.deleteItem:
                Cursor cusor = (Cursor) list.getItemAtPosition(info.position);
                db.deleteBook(cusor);
                populateListView();
                return true;
            default :
                return super.onContextItemSelected(item);
        }

    }
}
