package com.example.tp2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class BookDbHelper extends SQLiteOpenHelper {
    private static final String TAG = BookDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "book.db";

    public static final String TABLE_NAME = "library";

    public static final String _ID = "_id";
    public static final String COLUMN_BOOK_TITLE = "title";
    public static final String COLUMN_AUTHORS = "authors";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_GENRES = "genres";
    public static final String COLUMN_PUBLISHER = "publisher";
    public static final String[] COLUMN_NAMES = new String[]{"ROWID as _id",COLUMN_BOOK_TITLE,COLUMN_AUTHORS,COLUMN_YEAR,COLUMN_GENRES,COLUMN_PUBLISHER};
    public static final String CREATE_TABLE_LIBRARY = "CREATE TABLE library("
            + COLUMN_BOOK_TITLE + " TEXT,"
            + COLUMN_AUTHORS + " TEXT,"
            + COLUMN_YEAR + " TEXT,"
            + COLUMN_GENRES + " TEXT,"
            + COLUMN_PUBLISHER + " TEXT,"
            + " UNIQUE (" + COLUMN_BOOK_TITLE + ", " + COLUMN_AUTHORS + ") ON CONFLICT ROLLBACK )";

    public BookDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_LIBRARY);
        populate(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE "+TABLE_NAME);
        db.execSQL(CREATE_TABLE_LIBRARY);
    }


    /**
     * Adds a new book
     * @return  true if the book was added to the table ; false otherwise
     */
    public boolean addBook(Book book) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues cv = new ContentValues();

        cv.put(COLUMN_BOOK_TITLE,book.getTitle());
        cv.put(COLUMN_AUTHORS,book.getAuthors());
        cv.put(COLUMN_YEAR,book.getYear());
        cv.put(COLUMN_GENRES,book.getGenres());
        cv.put(COLUMN_PUBLISHER,book.getPublisher());

        long rowID = 0;
        rowID = db.insert(TABLE_NAME,COLUMN_BOOK_TITLE,cv);
        db.close(); // Closing database connection

        return (rowID != -1);
    }

    public boolean addBook(SQLiteDatabase db, Book book) {

        ContentValues cv = new ContentValues();
        cv.put(COLUMN_BOOK_TITLE,book.getTitle());
        cv.put(COLUMN_AUTHORS,book.getAuthors());
        cv.put(COLUMN_YEAR,book.getYear());
        cv.put(COLUMN_GENRES,book.getGenres());
        cv.put(COLUMN_PUBLISHER,book.getPublisher());

        long rowID = 0;
        rowID = db.insert(TABLE_NAME,COLUMN_BOOK_TITLE,cv);

        return (rowID != -1);
    }

    /**
     * Updates the information of a book inside the data base
     * @return the number of updated rows
     */
    public int updateBook(Book book) {
        SQLiteDatabase db = this.getWritableDatabase();
        int res=-45;
        // updating row
        // call db.update()

        ContentValues cv = new ContentValues();
        cv.put(COLUMN_BOOK_TITLE,book.getTitle());
        cv.put(COLUMN_AUTHORS,book.getAuthors());
        cv.put(COLUMN_YEAR,book.getYear());
        cv.put(COLUMN_GENRES,book.getGenres());
        cv.put(COLUMN_PUBLISHER,book.getPublisher());

        try {
            res=db.update(TABLE_NAME,cv,"ROWID = "+book.getId(),null);
        }catch (Exception e){

        }

        db.close();
        return res;
    }


    public Cursor fetchAllBooks() {
        SQLiteDatabase db = this.getReadableDatabase();
        // call db.query()
        Cursor cursor= db.query(TABLE_NAME,COLUMN_NAMES,null,null,null,null,COLUMN_BOOK_TITLE);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        db.close();
        return cursor;
    }

    public void deleteBook(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();
        // call db.delete();
        Long id = cursor.getLong(cursor.getColumnIndex("_id"));
        db.delete(TABLE_NAME,"ROWID = ?",new String[]{Long.toString(id)});
        db.close();
    }

    public void deleteALLBooks() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME,"",null);
        db.close();
    }

    public void populate(SQLiteDatabase db) {
        Log.d(TAG, "call populate()");
        addBook(db,new Book("Rouge Brésil", "J.-C. Rufin", "2003", "roman d'aventure, roman historique", "Gallimard"));
        addBook(db,new Book("Guerre et paix", "L. Tolstoï", "1865-1869", "roman historique", "Gallimard"));
        addBook(db,new Book("Fondation", "I. Asimov", "1957", "roman de science-fiction", "Hachette"));
        addBook(db,new Book("Du côté de chez Swan", "M. Proust", "1913", "roman", "Gallimard"));
        addBook(db,new Book("Le Comte de Monte-Cristo", "A. Dumas", "1844-1846", "roman-feuilleton", "Flammarion"));
        addBook(db,new Book("L'Iliade", "Homère", "8e siècle av. J.-C.", "roman classique", "L'École des loisirs"));
        addBook(db,new Book("Histoire de Babar, le petit éléphant", "J. de Brunhoff", "1931", "livre pour enfant", "Éditions du Jardin des modes"));
        addBook(db,new Book("Le Grand Pouvoir du Chninkel", "J. Van Hamme et G. Rosiński", "1988", "BD fantasy", "Casterman"));
        addBook(db,new Book("Astérix chez les Bretons", "R. Goscinny et A. Uderzo", "1967", "BD aventure", "Hachette"));
        addBook(db,new Book("Monster", "N. Urasawa", "1994-2001", "manga policier", "Kana Eds"));
        addBook(db,new Book("V pour Vendetta", "A. Moore et D. Lloyd", "1982-1990", "comics", "Hachette"));

    }

    public static Book cursorToBook(Cursor cursor) {
        Book book = null;

        Long id = cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
        String title = cursor.getString(cursor.getColumnIndexOrThrow("title"));
        String authors = cursor.getString(cursor.getColumnIndexOrThrow("authors"));
        String year = cursor.getString(cursor.getColumnIndexOrThrow("year"));
        String genres = cursor.getString(cursor.getColumnIndexOrThrow("genres"));
        String publisher = cursor.getString(cursor.getColumnIndexOrThrow("publisher"));

        book = new Book(id,title, authors, year, genres, publisher);

        return book;
    }

}
