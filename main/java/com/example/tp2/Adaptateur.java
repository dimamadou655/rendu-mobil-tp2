package com.example.tp2;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class Adaptateur extends ArrayAdapter<Book>{
    Context context;
    ArrayList<Book> books;

    public Adaptateur(Context context,ArrayList<Book> books) {
        super(context, 0, books);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Book book = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listeviw, parent, false);
        }
        // Lookup view for data population
        TextView titre = (TextView) convertView.findViewById(R.id.titre);
        TextView auteur = (TextView) convertView.findViewById(R.id.auteur);
        // Populate the data into the template view using the data object
        titre.setText(book.getTitle());
        auteur.setText(book.getAuthors());
        // Return the completed view to render on screen
        return convertView;
    }

}
